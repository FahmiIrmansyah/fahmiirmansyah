package com.example.smartspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.smartspring.model.PresidenModel;

public interface PresidenRepository  extends JpaRepository<PresidenModel, Integer>{
	
	@Query("select p from PresidenModel p where p.namaPresiden LIKE '%o%' or p.usiaPresiden=70")
	List<PresidenModel> querySelectUsia70atauMengandungHuruf0();

}
