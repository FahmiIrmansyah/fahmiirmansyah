package com.example.smartspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.smartspring.model.LaptopModel;
import com.example.smartspring.model.MahasiswaModel;

public interface LaptopRepository extends JpaRepository<LaptopModel, Integer>{
	
	@Query("select l from LaptopModel l")
	List<LaptopModel> reporead();
	
	@Query("select l from LaptopModel l where l.namaLaptop like %?1%")
	List<LaptopModel> searchNamaLaptop(String namaLaptop);

}
