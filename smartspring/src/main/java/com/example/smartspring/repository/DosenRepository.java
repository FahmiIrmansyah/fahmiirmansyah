package com.example.smartspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.smartspring.model.DosenModel;


public interface DosenRepository extends JpaRepository<DosenModel, String>{
	
@Query("select d from DosenModel d")
List<DosenModel> reporead();

@Query("select d from DosenModel d ORDER BY d.namaDosen DESC")
List<DosenModel> querySelectAllOrderNamaDesc();

@Query("select d from DosenModel d where d.namaDosen LIKE %?1%")
List<DosenModel> querySelectAllWhereNamaLike(String katakunci_nama);

@Query("select d from DosenModel d order by d.usiaDosen ASC")
List<DosenModel> querySelectAllOrderUsiaAsc();

@Query("select d from DosenModel d where d.usiaDosen = ?1 and d.namaDosen LIKE ?2%")
List<DosenModel> querySelectAllWhereUsiaEqual(int katakunci_usia,String katakunci_awalan);

@Query("select d from DosenModel d where d.usiaDosen > ?1 and d.namaDosen LIKE ?2%")
List<DosenModel> querySelectAllWhereUsiaMoreThan(int katakunci_usia,String katakunci_awalan);

@Query("select d from DosenModel d where d.usiaDosen < ?1 and d.namaDosen LIKE ?2%")
List<DosenModel> querySelectAllWhereUsiaLessThan(int katakunci_usia,String katakunci_awalan);

@Query("select d from DosenModel d where d.usiaDosen >= ?1 and d.namaDosen LIKE ?2%")
List<DosenModel> querySelectAllWhereUsiaMoreThanEqual(int katakunci_usia,String katakunci_awalan);

@Query("select d from DosenModel d where d.usiaDosen <= ?1 and d.namaDosen LIKE ?2%")
List<DosenModel> querySelectAllWhereUsiaLessThanEqual(int katakunci_usia,String katakunci_awalan);

@Query("select d from DosenModel d where d.usiaDosen != ?1 and d.namaDosen LIKE ?2%")
List<DosenModel> querySelectAllWhereUsiaNotEqual(int katakunci_usia,String katakunci_awalan);

@Query("select d from DosenModel d where d.namaDosen = ?1")
DosenModel queryWhereNamaDosen(String namaDosen);

@Query("select d from DosenModel d where d.namaDosen like 'a%'")
List<DosenModel> queryWhereSelectAllWhereUsiaMoreThan20();

@Query("select d from DosenModel d where d.usiaDosen < 25")
List<DosenModel> queryWhereSelectUsiaLessThan();

}
