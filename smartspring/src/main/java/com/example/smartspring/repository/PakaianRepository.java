package com.example.smartspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.smartspring.model.FakultasModel;
import com.example.smartspring.model.PakaianModel;

public interface PakaianRepository extends JpaRepository<PakaianModel,String>{

}
