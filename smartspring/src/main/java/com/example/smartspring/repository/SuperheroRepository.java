package com.example.smartspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.smartspring.model.SuperheroModel;

public interface SuperheroRepository extends JpaRepository<SuperheroModel, String> {

}
