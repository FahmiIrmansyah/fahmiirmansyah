package com.example.smartspring.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.smartspring.model.MahasiswaModel;
import com.example.smartspring.model.PasienModel;

public interface MahasiswaRepository extends JpaRepository<MahasiswaModel, String>{
	
	@Query("select m from MahasiswaModel m")
	List<MahasiswaModel> reporead();
	
	@Query("select m from MahasiswaModel m where m.namaMahasiswa like %?1%")
    List<MahasiswaModel> searchNamaMahasiswa(String namaMahasiswa);

}
