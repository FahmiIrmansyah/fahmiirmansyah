package com.example.smartspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.smartspring.model.ProdukModel;

public interface ProdukRepository extends JpaRepository<ProdukModel,String> {

	@Query("select p from ProdukModel p where p.namaProduk like %?1%")
	List<ProdukModel> cariNamaProduk(String cari);
	
	@Query("select p from ProdukModel p where p.kodeProduk like %?1%")
	List<ProdukModel> cariNamaKode(String cari);
	
	@Query("select p from ProdukModel p order by p.hargaProduk asc")
	List<ProdukModel> hargaProdukAsc();
}
