package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MAHASISWA")
public class MahasiswaModel {
	@Id
	@Column(name = "_T_NPM")
	private String NPM;

	@Column(name = "_T_namaMahasiswa")
	private String namaMahasiswa;

	@Column(name = "_T_kelas")
	private String kelas;

	@Column(name = "_T_semester")
	private int semester;

	public String getNPM() {
		return NPM;
	}

	public void setNPM(String NPM) {
		this.NPM = NPM;
	}

	public String getNamaMahasiswa() {
		return namaMahasiswa;
	}

	public void setNamaMahasiswa(String namaMahasiswa) {
		this.namaMahasiswa = namaMahasiswa;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

}
