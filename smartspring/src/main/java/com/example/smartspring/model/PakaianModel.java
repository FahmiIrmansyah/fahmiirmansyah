package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_PAKAIAN")
public class PakaianModel {
	
	@Id
	@Column(name="JENIS_PAKAIAN")
	public String jenisPakaian;
	
	@Column(name="HARGA_PAKAIAN")
	public int hargaPakaian;
	
	public String getJenisPakaian() {
		return jenisPakaian;
	}
	public void setJenisPakaian(String jenisPakaian) {
		this.jenisPakaian = jenisPakaian;
	}
	public int getHargaPakaian() {
		return hargaPakaian;
	}
	public void setHargaPakaian(int hargaPakaian) {
		this.hargaPakaian = hargaPakaian;
	}
	
	

}
