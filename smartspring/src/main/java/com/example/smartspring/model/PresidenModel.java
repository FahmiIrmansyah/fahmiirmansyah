package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_PRESIDEN")
public class PresidenModel {
	
	@Id
	@Column(name="CLM_NOMOR")
	int nomor;
	
	@Column(name="CLM_NAMA_PRESIDEN")
	String namaPresiden;
	
	@Column(name="CLM_KOTA_PRESIDEN")
	String kotaPresiden;
	
	@Column(name="CLM_USIA_PRESIDEN")
	int usiaPresiden;
	

	public int getNomor() {
		return nomor;
	}
	public void setNomor(int nomor) {
		this.nomor = nomor;
	}
	public String getNamaPresiden() {
		return namaPresiden;
	}
	public void setNamaPresiden(String namaPresiden) {
		this.namaPresiden = namaPresiden;
	}
	public String getKotaPresiden() {
		return kotaPresiden;
	}
	public void setKotaPresiden(String kotaPresiden) {
		this.kotaPresiden = kotaPresiden;
	}
	public int getUsiaPresiden() {
		return usiaPresiden;
	}
	public void setUsiaPresiden(int usiaPresiden) {
		this.usiaPresiden = usiaPresiden;
	}

	
}
