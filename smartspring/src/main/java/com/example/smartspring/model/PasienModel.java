package com.example.smartspring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="T_PASIEN")
public class PasienModel {

@Id
@Column(name="_T_NO")	
private String noPasien;

@Column(name="_T_NAMA")
private String namaPasien;

@Column(name="_T_GENDER")
private String gender;

@Column(name="_T_KATEGORY")
private String kategory;

@Column(name="_T_BIAYA")
private int biaya;

@Column(name="_TANGGAL")
private Date tanggal;

public String getNoPasien() {
	return noPasien;
}
public void setNoPasien(String noPasien) {
	this.noPasien = noPasien;
}
public String getNamaPasien() {
	return namaPasien;
}
public void setNamaPasien(String namaPasien) {
	this.namaPasien = namaPasien;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getKategory() {
	return kategory;
}
public void setKategory(String kategory) {
	this.kategory = kategory;
}
public int getBiaya() {
	return biaya;
}
public void setBiaya(int biaya) {
	this.biaya = biaya;
}
public Date getTanggal() {
	return tanggal;
}
public void setTanggal(Date tanggal) {
	this.tanggal = tanggal;
}

 


}
