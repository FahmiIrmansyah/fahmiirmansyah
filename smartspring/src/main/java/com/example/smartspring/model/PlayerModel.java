package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_PLAYER")
public class PlayerModel {

	@Id
	@Column(name="NO_PLAYER")
	String noPlayer;
	
	@Column(name="NAMA_PLAYER")
	String namaPlayer;
	
	@Column(name="LEVEL_PLAYER")
	int  levelPlayer;
	
	public String getNoPlayer() {
		return noPlayer;
	}
	public void setNoPlayer(String noPlayer) {
		this.noPlayer = noPlayer;
	}
	public String getNamaPlayer() {
		return namaPlayer;
	}
	public void setNamaPlayer(String namaPlayer) {
		this.namaPlayer = namaPlayer;
	}
	public int getLevelPlayer() {
		return levelPlayer;
	}
	public void setLevelPlayer(int levelPlayer) {
		this.levelPlayer = levelPlayer;
	}
	
	
}
