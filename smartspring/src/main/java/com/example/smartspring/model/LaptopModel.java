package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_LAPTOP")
public class LaptopModel {

	@Id
	@Column(name = "_T_SerialNumber")
	private int serialNumber;

	@Column(name = "_T_namaLaptop")
	private String namaLaptop;

	@Column(name = "_T_seriLaptop")
	private String seriLaptop;

	@Column(name = "_T_Spesifikasi")
	private String spesifikasi;

	@Column(name = "_T_harga")
	private int harga;

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getNamaLaptop() {
		return namaLaptop;
	}

	public void setNamaLaptop(String namaLaptop) {
		this.namaLaptop = namaLaptop;
	}

	public String getSeriLaptop() {
		return seriLaptop;
	}

	public void setSeriLaptop(String seriLaptop) {
		this.seriLaptop = seriLaptop;
	}

	public String getSpesifikasi() {
		return spesifikasi;
	}

	public void setSpesifikasi(String spesifikasi) {
		spesifikasi = spesifikasi;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

}
