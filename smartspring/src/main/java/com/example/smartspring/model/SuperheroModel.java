package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_SUPERHERO")
public class SuperheroModel {
	
	@Id
	@Column(name="CLM_NAMA")
	String nama;
	
	@Column(name="CLM_LEVEL")	
	int level;
	
	@Column(name="CLM_STATUS")
	String status;
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
