package com.example.smartspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_FAKULTAS")
public class FakultasModel {

	@Id
	@Column(name="KODE_JURUSAN")
	String kodeJurusan ;

	@Column(name="JURUSAN")
	String jurusan ;
	
	@Column(name="FAKULTAS")
	String fakultas ;
	
	public String getKodeJurusan() {
		return kodeJurusan;
	}
	public void setKodeJurusan(String kodeJurusan) {
		this.kodeJurusan = kodeJurusan;
	}
	public String getJurusan() {
		return jurusan;
	}
	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}
	public String getFakultas() {
		return fakultas;
	}
	public void setFakultas(String fakultas) {
		this.fakultas = fakultas;
	}
	
	
}
