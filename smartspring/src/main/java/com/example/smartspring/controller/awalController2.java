package com.example.smartspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class awalController2 {
	
	@RequestMapping("/utama2")
	public String utama2(HttpServletRequest request, Model model ) {
		String textCtrl = request.getParameter("text");
		model.addAttribute("textLempar",textCtrl);
		String utama = "utama2";
		return utama;
	}
	
	@RequestMapping("/login2")
	public String login2() {
		String login = "login2";
		return login;
	}

}
