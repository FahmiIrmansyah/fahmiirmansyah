package com.example.smartspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.ProdukModel;
import com.example.smartspring.service.ProdukService;

@Controller
public class ProdukController {

	@Autowired
	private ProdukService produkService;
	
	@RequestMapping(value= "menu_tambah_produk")
	public String menuTambahProduk() {
		String html = "produk/tambah_produk";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_produk")
	public String prosesTambahProduk(HttpServletRequest request,Model model) {
		String kode = request.getParameter("kode");
		String nama = request.getParameter("nama");
		int harga = Integer.valueOf(request.getParameter("harga"));
		ProdukModel produkModel = new ProdukModel();
		produkModel.setKodeProduk(kode);
		produkModel.setNamaProduk(nama);
		produkModel.setHargaProduk(harga);
		produkService.create(produkModel);
		model.addAttribute("produkModel", produkModel);
		String html = "produk/sukses";
		return html;		
	}
	
	@RequestMapping(value="tampil_produk")
	public String tampilProduk(HttpServletRequest request, Model model) {
		String kode = request.getParameter("kode");
		String nama = request.getParameter("nama");
		int harga = Integer.valueOf(request.getParameter("harga"));
		ProdukModel produkModel = new ProdukModel();
		produkModel.setKodeProduk(kode);
		produkModel.setNamaProduk(nama);
		produkModel.setHargaProduk(harga);
		produkService.create(produkModel);
		List<ProdukModel> produkModelList = new ArrayList<>();
		produkModelList = produkService.tampilProduk();
		model.addAttribute("produkModelList",produkModelList);
		return "produk/tampilproduk";
	}
	
	@RequestMapping(value="cari_produk")
	public String cariProduk(HttpServletRequest request,Model model) {
		String cariNama = request.getParameter("cariNama");
		List<ProdukModel> modelProdukList = new ArrayList<>();
		modelProdukList = produkService.cariProduk(cariNama);
		model.addAttribute("produkModelList",modelProdukList);
		return "produk/tampilproduk";
	}
	
	@RequestMapping(value="cari_kode")
	public String cariKode(HttpServletRequest request,Model model) {
		String cariKode = request.getParameter("cariKode");
		List<ProdukModel> modelProdukList = new ArrayList<ProdukModel>();
		modelProdukList = produkService.cariKode(cariKode);
		model.addAttribute("produkModelList",modelProdukList);
		return "produk/tampilproduk";
	}
	
	@RequestMapping(value="menu_daftar_produk")
	public String menuDaftarProduk(Model model) {
	    List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
	    produkModelList = produkService.tampilProduk();
	    model.addAttribute("produkModelList",produkModelList);
		return "produk/daftarproduk";
	}
	
	@RequestMapping(value="menu_urut_asc_produk")
	public String menuUrutAscProduk(Model model) {
	    List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
	    produkModelList = produkService.urutHarga();
	    model.addAttribute("produkModelList",produkModelList);
		return "produk/urut_asc_produksi";
	}
}
