package com.example.smartspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.DosenModel;
import com.example.smartspring.service.DosenService;

@Controller
public class DosenController {

	@Autowired
	private DosenService ds;

	@RequestMapping(value = "tambah_dosen")
	public String menuIsiDosen() {
		String html = "dosen/isi_dosen";
		return html;
	}

	@RequestMapping(value = "hasil_satu_dosen")
	public String menuSatuDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));

		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);

		// transaksi simpan c create
		model.addAttribute("dosenModel", dosenModel);

		String html = "dosen/satu_dosen";
		return html;
	}

	@RequestMapping(value = "hasil_banyak_dosen")
	public String menuBanyakDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		DosenModel dm = new DosenModel();
		dm.setNamaDosen(namaDosen);
		dm.setUsiaDosen(usiaDosen);
		this.ds.create(dm);
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = ds.read();
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/banyak_dosen";
		return html;
	}

	@RequestMapping(value = "hasil_banyak_dosen_order_name")
	public String menuBanyakDosenOrderNama(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		DosenModel dm = new DosenModel();
		dm.setNamaDosen(namaDosen);
		dm.setUsiaDosen(usiaDosen);
		this.ds.create(dm);
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = ds.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/banyak_dosen_nama_asc";
		return html;
	}

	@RequestMapping(value = "cari_nama_dosen")
	public String menuCariNamaDosen(HttpServletRequest request, Model model) {
        List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = ds.read();
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/cari_nama_dosen";
		return html;
	}

	@RequestMapping(value = "proses_cari_nama")
	public String menuProsesCariNamaDosen(HttpServletRequest request, Model model) {
		String katakunci_nama = request.getParameter("katakunci_namaDosen");
		List<DosenModel> dosenModelList = new ArrayList<>();
		dosenModelList = ds.readWhereNama(katakunci_nama);
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/cari_nama_dosen";
		return html;
	}

	@RequestMapping(value = "cari_usia_dosen")
	public String menuCariUsiaDosen(HttpServletRequest request, Model model) {
		List<DosenModel> dosenModelList = new ArrayList();
		dosenModelList = ds.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/cari_usia";
		return html;
	}

	@RequestMapping(value = "proses_cari_usia")
	public String menuProsesCariUsia(HttpServletRequest request, Model model) {
		int katakunci_usia = Integer.valueOf(request.getParameter("katakunci_usia"));
		String katakunci_operator = request.getParameter("operator");
		String katakunci_awalan = request.getParameter("awalan");
		List<DosenModel> dosenModelList = new ArrayList<>();
		dosenModelList = ds.readWhereUsiaByOperator(katakunci_usia, katakunci_operator,katakunci_awalan);
		model.addAttribute("dosenModelList", dosenModelList);
		String html = "dosen/cari_usia";
		return html;
	}

	@RequestMapping(value = "ubah_dosen")
	public String menuUbahDosen(HttpServletRequest request, Model model) {
		String namaID = request.getParameter("namaID");
		DosenModel dosenModel = new DosenModel();
		dosenModel = ds.readByID(namaID);
		model.addAttribute("dosenModel", dosenModel);
		String html = "dosen/ubah_dosen";
		return html;
	}
	
	@RequestMapping(value= "proses_ubah_dosen")
	public String menuProsesUbahDosen(HttpServletRequest request,Model model) {
		String namaDosen = request.getParameter("namaID");
		int usiaDosen = Integer.valueOf(request.getParameter("usiaID"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		ds.create(dosenModel);
		readData(model);
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	public void readData(Model model) {
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = ds.readOrderNama();
		model.addAttribute("dosenModelList",dosenModelList);
	}

	@RequestMapping(value = "hapus_dosen")
	public String menuHapusDosen(HttpServletRequest request, Model model) {
		String namaID = request.getParameter("namaID");
		DosenModel dosenModel = new DosenModel();
		dosenModel = ds.readByID(namaID);
		model.addAttribute("dosenModel", dosenModel);
		String html = "dosen/hapus_dosen";
		return html;
	}
	
	@RequestMapping(value = "proses_hapus_dosen")
	public String menuProsesHapusDosen (HttpServletRequest request, Model model) {
		String namaID = request.getParameter("namaID");
		ds.delete(namaID);
		readData(model);
		return "dosen/banyak_dosen";
	}
	

}
