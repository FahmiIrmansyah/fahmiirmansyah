package com.example.smartspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrisaktiController {
	
	@RequestMapping(value="trisakti")
	public String methodTrisakti() {
		String html = "/Trisakti/trisakti";
		return html;
	}
	
	@RequestMapping(value="trisaktihasil")
	public String methodTrisaktiHasil(HttpServletRequest request, Model model) {
		String NamaLengkap = request.getParameter("namaLengkap");
		String TempatLahir = request.getParameter("tempatLahir");
		String TanggalLahir = request.getParameter("tanggalLahir");
		String JenisKelamin = request.getParameter("jenisKelamin");
		String Agama = request.getParameter("agama");
		String Telepon = request.getParameter("telepon");
		String Hp = request.getParameter("hp");
		String Kewarganegaraan = request.getParameter("kewarganegaraan");
		String Nik = request.getParameter("nik");
		String GolonganDarah = request.getParameter("golonganDarah");
		String TinggalDiJakarta = request.getParameter("tinggalDiJakarta");
		String AnakKe = request.getParameter("anakKe");
		String Bersaudara = request.getParameter("bersaudara");
		String StatusNikah = request.getParameter("statusNikah");
		
		model.addAttribute("hasilNamaLengkap",NamaLengkap);
		model.addAttribute("hasilTempatLahir",TempatLahir);
		model.addAttribute("hasilTanggalLahir",TanggalLahir);
		model.addAttribute("hasilJenisKelamin",JenisKelamin);
		model.addAttribute("hasilAgama",Agama);
		model.addAttribute("hasilTelepon",Telepon);
		model.addAttribute("hasilHp",Hp);
		model.addAttribute("hasilKewarganegaraan",Kewarganegaraan);
		model.addAttribute("hasilNik",Nik);
		model.addAttribute("hasilGolonganDarah",GolonganDarah);
		model.addAttribute("hasilTinggalDiJakarta",TinggalDiJakarta);
		model.addAttribute("hasilAnakKe",AnakKe);
		model.addAttribute("hasilBersaudara",Bersaudara);
		model.addAttribute("hasilStatusNikah",StatusNikah);
		
		String html = "/Trisakti/trisaktihasil";
		return html;
	}

}
