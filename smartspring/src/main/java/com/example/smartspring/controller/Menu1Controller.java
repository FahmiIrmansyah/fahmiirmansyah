package com.example.smartspring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Menu1Controller {

	@RequestMapping(value = "/menu1")
	public String menu1() {
		String menu1 = "/menu/menu1";
		return menu1;
	}

	@RequestMapping(value = "/menu2")
	public String menu2() {
		String menu2 = "/menu/menu2";
		return menu2;
	}
	
	@RequestMapping(value = "/menu3")
	public String menu3() {
		String menu2 = "/menu/menu3";
		return menu2;
	}
}
