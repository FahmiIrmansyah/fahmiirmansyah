package com.example.smartspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.SuperheroModel;

@Controller
public class SuperHeroController {

	
	@RequestMapping(value="menusuperhero")
	public String methodMenuSuperhero() {
		String html = "/superhero/InputSuperhero";
		return html;
	}
	
	@RequestMapping(value="hasilsuperhero")
	public String methodHasilSuperhero(HttpServletRequest request,Model model) {
		String nama = request.getParameter("nama");
		int level = Integer.valueOf(request.getParameter("level"));
		String status = request.getParameter("status");
		
		SuperheroModel sm = new SuperheroModel();
		sm.setNama(nama);
		sm.setLevel(level);
		sm.setStatus(status);
		
		model.addAttribute("sm",sm);
		String html = "/superhero/HasilSuperhero";
		return html;
	}
	
	
}
