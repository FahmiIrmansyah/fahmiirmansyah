package com.example.smartspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.PakaianModel;
import com.example.smartspring.service.PakaianService;

@Controller
public class PakaianController {
	@Autowired
	PakaianService ps;

	@RequestMapping(value = "isi_pakaian")
	public String menuIsiPakaian() {
		return "Pakaian/isipakaian";
	}

	@RequestMapping(value = "proses_tambah_pakaian")
	public String menuProsesTambahPakaian(HttpServletRequest request, Model model) {
		String jenisPakaian = request.getParameter("jenispakaian");
		int hargaPakaian = Integer.valueOf(request.getParameter("hargapakaian"));
		PakaianModel pm = new PakaianModel();
		pm.setJenisPakaian(jenisPakaian);
		pm.setHargaPakaian(hargaPakaian);
		ps.create(pm);
		return "Pakaian/daftarpakaian";
	}

}
