package com.example.smartspring.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.PasienModel;
import com.example.smartspring.service.PasienService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public class PasienController {
    @Autowired
    private PasienService pasienService;
    
	@RequestMapping(value = "/pasien/create")
	public String pasienCreate(HttpServletRequest request, Model model) throws ParseException {
		String nomorPasien = request.getParameter("nomor");
		String namaPasien = request.getParameter("nama");
		String gender = request.getParameter("namaGender");
		String kategory = request.getParameter("kategory");
		int biayaPasien = Integer.parseInt(request.getParameter("biaya"));
		String tanggalLahir = request.getParameter("tanggalLahir");
		System.out.println(tanggalLahir);
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		Date tanggalLahirDate = formatDate.parse(tanggalLahir);
		System.out.println(tanggalLahirDate);

		// simpan ke masing2 kolom di pasien model
		PasienModel pasienModel = new PasienModel();
		pasienModel.setNoPasien(nomorPasien);
		pasienModel.setNamaPasien(namaPasien);
		pasienModel.setKategory(kategory);
		pasienModel.setBiaya(biayaPasien);
		pasienModel.setGender(gender);
		pasienModel.setTanggal(tanggalLahirDate);
		
		// simpan ke database
		this.pasienService.create(pasienModel);
		this.pasienRead(model);
		
		String html = "/pasien/data";
		return html;
	}
	
	public void pasienRead(Model model) {
		List<PasienModel> pasienModelList = new ArrayList<PasienModel>();
		pasienModelList = pasienService.read();
		model.addAttribute("pasienModelList", pasienModelList);
	}
	
	@RequestMapping(value="/pasien/search/nama")
	public String cariNama (HttpServletRequest request,Model model) {
		String namaPasien = request.getParameter("cari");
		List<PasienModel> pasienModelList = new ArrayList<PasienModel>();
		pasienModelList = this.pasienService.searchNama(namaPasien);
		model.addAttribute("pasienModelList", pasienModelList);
		String html = "/pasien/data";
		return html;
	}
}
