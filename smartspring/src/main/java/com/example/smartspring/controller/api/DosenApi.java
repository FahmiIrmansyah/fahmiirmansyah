package com.example.smartspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.smartspring.model.DosenModel;
import com.example.smartspring.repository.DosenRepository;
import com.example.smartspring.service.DosenService;

@RestController
@RequestMapping("/api/DosenApi")
public class DosenApi {

	@Autowired
	private DosenService dosenService;
	
	@Autowired
	private DosenRepository dosenRepository;
	
	
	  @GetMapping("/get")
	  @ResponseStatus(code=HttpStatus.OK) 
	  public List<DosenModel> getapi(){
	  List<DosenModel>dosenModelList = new ArrayList<DosenModel>(); 
	  dosenModelList = this.dosenService.read();
	  return dosenModelList;
	  }
	  
	  @GetMapping("/usialessthan")
	  @ResponseStatus(code=HttpStatus.OK) 
	  public List<DosenModel> usialessthan(){
	  List<DosenModel>dosenModelList = new ArrayList<DosenModel>(); 
	  dosenModelList = this.dosenService.readUsiaDuaLima();
	  return dosenModelList;
	  }
	  
	  @PostMapping("/post")
	  @ResponseStatus(code=HttpStatus.CREATED)
	  public Map<String,Object> postApi(@RequestBody DosenModel dosenModel){
		  this.dosenService.create(dosenModel);
		  Map<String,Object> map = new HashMap<String,Object>();
		  map.put("success",Boolean.TRUE);
		  map.put("pesan","selamat data anda berhasil dimasukkan");
		  return map;
	  }
	  
	  @PutMapping("/put")
	  @ResponseStatus(code=HttpStatus.OK)
	  public Map<String,Object> putApi(@RequestBody DosenModel dosenModel){
		  this.dosenService.update(dosenModel);
		  Map<String,Object> map = new HashMap<String,Object>();
		  map.put("success",Boolean.TRUE);
		  map.put("pesan","data berhasi diupdate");
		  return map;
	  }
	  
	  @DeleteMapping("/delete/{namaDosen}")
	  @ResponseStatus(code=HttpStatus.GONE)
	  public Map<String, Object> deleteApi(@PathVariable String namaDosen){
		  this.dosenService.delete(namaDosen);
		  Map<String,Object> map = new HashMap<String,Object>();
		  map.put("Success", Boolean.TRUE);
		  map.put("Success", "Selamat Data Anda "+namaDosen+" telah dihapus");
		  return map;
	  }
	
	
}
