package com.example.smartspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class awalController {

	@RequestMapping("/")
	public String login() {
		String login = "login";
		return login;
	}
	
	@RequestMapping("/beranda")
	public String beranda() {
		String html = "utama";
		return html;
	}
	
	@RequestMapping("/login/action")
	public String methodLoginAction(HttpServletRequest request, Model model) {
		String akunCtrl = request.getParameter("akunHtml");
		String passwordCtrl = request.getParameter("passwordHtml");
		model.addAttribute("akunLempar",akunCtrl);
		model.addAttribute("passwordLempar",passwordCtrl);
		
		String html = "utama";
		return html;
		//HttpServletRequest untuk melempar data dari front end ke back end
		//model untuk mengembalikan data dari back end ke front end
	}
}
