package com.example.smartspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.MahasiswaModel;
import com.example.smartspring.service.MahasiswaService;

@Controller
public class MahasiswaController {
	
	@Autowired
	private MahasiswaService mahasiswaService;
	
	@RequestMapping(value="/mahasiswa/create")
	public String mahasiswaCreate(HttpServletRequest request, Model model) {
		String npm = request.getParameter("npm");
		String namaMahasiswa = request.getParameter("namaMahasiswa");
		String kelas = request.getParameter("kelas");
		int semester = Integer.parseInt(request.getParameter("semester"));
		
		MahasiswaModel mahasiswaModel = new MahasiswaModel ();
		mahasiswaModel.setNPM(npm);
		mahasiswaModel.setNamaMahasiswa(namaMahasiswa);
		mahasiswaModel.setKelas(kelas);
		mahasiswaModel.setSemester(semester);
		
		this.mahasiswaService.create(mahasiswaModel);
		this.mahasiswaRead(model);
		String html ="/mahasiswa/data2";
		return html;
	}

	public void mahasiswaRead(Model model) {
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = mahasiswaService.read();
		model.addAttribute("mahasiswaModelList", mahasiswaModelList);
	}
	
	@RequestMapping(value="/mahasiswa/search/nama")
	public String cariNama (HttpServletRequest request,Model model) {
		String namaMahasiswa = request.getParameter("cari");
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = this.mahasiswaService.searchNama(namaMahasiswa);
		model.addAttribute("mahasiswaModelList", mahasiswaModelList);
		String html = "/mahasiswa/data2";
		return html;
	}
}
