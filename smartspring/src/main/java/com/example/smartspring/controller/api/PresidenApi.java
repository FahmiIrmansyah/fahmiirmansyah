package com.example.smartspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.smartspring.model.DosenModel;
import com.example.smartspring.model.PresidenModel;
import com.example.smartspring.service.PresidenService;

@RestController
@RequestMapping("/api/PresidenApi")
public class PresidenApi {
	
	@Autowired
	private PresidenService presidenService;

	  @PostMapping("/post")
	  @ResponseStatus(code=HttpStatus.CREATED)
	  public Map<String,Object> postApi(@RequestBody PresidenModel presidenModel){
		  this.presidenService.create(presidenModel);
		  Map<String,Object> map = new HashMap<String,Object>();
		  map.put("success",Boolean.TRUE);
		  map.put("pesan","selamat data anda berhasil dimasukkan");
		  return map;
	  }
	  
	  @GetMapping("/get")
	  @ResponseStatus(code=HttpStatus.OK) 
	  public List<PresidenModel> getapi(){
	  List<PresidenModel>presidenModelList = new ArrayList<PresidenModel>(); 
	  presidenModelList = this.presidenService.read();
	  return presidenModelList;
	  }
	  
	  @GetMapping("/get2")
	  @ResponseStatus(code=HttpStatus.OK) 
	  public List<PresidenModel> getapi2(){
	  List<PresidenModel>presidenModelList = new ArrayList<PresidenModel>(); 
	  presidenModelList = this.presidenService.read2();
	  return presidenModelList;
	  }
}
