package com.example.smartspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.PlayerModel;
import com.example.smartspring.service.PlayerService;

@Controller
public class PlayerController {

	@Autowired
	PlayerService ps;
	
	@RequestMapping(value="menu_isi_player")
	public String menuIsiPlayer() {
		return "Player/isiplayer";
	}
	
	@RequestMapping(value="proses_isi_player")
	public String prosesIsiPlayer(HttpServletRequest request) {
		String noPlayer = request.getParameter("noplayer");
		String namaPlayer = request.getParameter("namaplayer");
		int levelPlayer = Integer.valueOf(request.getParameter("levelplayer"));
		PlayerModel pm = new PlayerModel();
		pm.setNoPlayer(noPlayer);
		pm.setNamaPlayer(namaPlayer);
		pm.setLevelPlayer(levelPlayer);
		ps.create(pm);
		return "Player/isiplayer";
	}
	
	@RequestMapping(value="proses_isi_player_tanpa_list")
	public String prosesIsiPlayerTanpaList(HttpServletRequest request,Model model) {
		String noPlayer = request.getParameter("noplayer");
		String namaPlayer = request.getParameter("namaplayer");
		int levelPlayer = Integer.valueOf(request.getParameter("levelplayer"));
		PlayerModel pm = new PlayerModel();
		pm.setNoPlayer(noPlayer);
		pm.setNamaPlayer(namaPlayer);
		pm.setLevelPlayer(levelPlayer);
		model.addAttribute("hasilnoplayer",noPlayer);
		model.addAttribute("hasilnamaplayer",namaPlayer);
		model.addAttribute("hasillevelplayer",levelPlayer);
		ps.create(pm);
		return "Player/isiplayer";
	}
	
	@RequestMapping(value="daftar_player")
	public String daftarPlayer(Model model) {
		List<PlayerModel> pml = new ArrayList<>();
		pml = ps.read();
		model.addAttribute("pml",pml);
		return "Player/daftarplayer";
	}
}
