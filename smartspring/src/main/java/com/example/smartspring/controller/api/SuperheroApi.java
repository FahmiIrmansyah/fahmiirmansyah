package com.example.smartspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.smartspring.model.DosenModel;
import com.example.smartspring.model.SuperheroModel;
import com.example.smartspring.repository.SuperheroRepository;
import com.example.smartspring.service.SuperheroService;

@RestController
@RequestMapping("/api/SuperheroApi")
public class SuperheroApi {

	@Autowired
	private SuperheroService ss;
	
	@Autowired
    private SuperheroRepository sr;
	
	@PostMapping("/post")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Map<String,Object> postApi(@RequestBody SuperheroModel sm){
		this.ss.create(sm);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("success", Boolean.TRUE);
		map.put("pesan", "Data berhasil dimasukkan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code=HttpStatus.CREATED)
	public List<SuperheroModel> getApi(){
		List<SuperheroModel>superheroModelList = new ArrayList<SuperheroModel>();
		superheroModelList = ss.read();
		return superheroModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code=HttpStatus.OK)
	public Map<String,Object> putApi(@RequestBody SuperheroModel sm){
		this.ss.update(sm);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("success", Boolean.TRUE);
		map.put("pesan", "Data berhasil diedit");
		return map;
	}
	
	@DeleteMapping("/delete/{nama}")
	@ResponseStatus(code=HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String nama){
		this.ss.delete(nama);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("Success", Boolean.TRUE);
		map.put("Success", "Data " +nama+" berhasil dihapus");
		return map;
	}
}
