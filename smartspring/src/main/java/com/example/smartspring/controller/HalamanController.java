package com.example.smartspring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HalamanController {

	@RequestMapping("/linkganti")
	public String methodPanggilGanti() {
		String html = "ganti";
		return html;
	}
	
	@RequestMapping("/linkbaru")
	public String methodPanggilBaru() {
		String html = "baru";
		return html;
	}
	
	@RequestMapping(value="/linkcss")
	public String methodCss() {
		String html = "BelajarCSS/ContohCSS";
		return html;
	}
}
