package com.example.smartspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.LaptopModel;

import com.example.smartspring.service.LaptopService;

@Controller
public class LaptopController {

	@Autowired
	private LaptopService laptopService;

	@RequestMapping(value = "/laptop/create")
	public String laptopCreate(HttpServletRequest request, Model model) {
		int SerialNumber = Integer.parseInt(request.getParameter("serialNumber"));
		String NamaLaptop = request.getParameter("namaLaptop");
		String SeriLaptop = request.getParameter("seriLaptop");
		String Spesifikasi = request.getParameter("spesifikasi");
		int harga = Integer.parseInt(request.getParameter("harga"));

		LaptopModel laptopModel = new LaptopModel();
		laptopModel.setSerialNumber(SerialNumber);
		laptopModel.setNamaLaptop(NamaLaptop);
		laptopModel.setSeriLaptop(SeriLaptop);
		laptopModel.setSpesifikasi(Spesifikasi);
		laptopModel.setHarga(harga);

		this.laptopService.create(laptopModel);
		this.laptopRead(model);
		String html = "/laptop/data3";
		return html;
	}

	public void laptopRead(Model model) {
		List<LaptopModel> laptopModelList = new ArrayList<LaptopModel>();
		laptopModelList = laptopService.read();
		model.addAttribute("laptopModelList", laptopModelList);
	}

	@RequestMapping(value = "/laptop/search/nama")
	public String cariNama(HttpServletRequest request, Model model) {
		String namaLaptop = request.getParameter("cari");
		List<LaptopModel> laptopModelList = new ArrayList<LaptopModel>();
		laptopModelList = this.laptopService.searchNama(namaLaptop);
		model.addAttribute("laptopModelList", laptopModelList);
		String html = "/laptop/data3";
		return html;
	}
}
