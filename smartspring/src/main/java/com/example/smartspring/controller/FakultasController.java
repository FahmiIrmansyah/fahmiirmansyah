package com.example.smartspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.smartspring.model.FakultasModel;
import com.example.smartspring.service.FakultasService;

@Controller
public class FakultasController {
	@Autowired
	FakultasService fs;

	@RequestMapping(value = "tambah_fakultas")
	public String menuTambahFakultas() {
		return "Fakultas/isiFakultas";
	}

	@RequestMapping(value = "proses_tambah_fakultas")
	public String menuProsesTambahFakultas(HttpServletRequest request) {
		String kodejurusan = request.getParameter("kodejurusan");
		String jurusan = request.getParameter("jurusan");
		String fakultas = request.getParameter("fakultas");
		FakultasModel fm = new FakultasModel();
		fm.setKodeJurusan(kodejurusan);
		fm.setJurusan(jurusan);
		fm.setFakultas(fakultas);
		fs.create(fm);
		return "Fakultas/IsiFakultas";
	}
	
	@RequestMapping(value= "baca_fakultas")
	public String menuDaftarFakultas(Model model) {
		List<FakultasModel> lfm = new ArrayList<>();
		lfm = fs.read();
		model.addAttribute("fakultasModelList", lfm);
	   return "Fakultas/DaftarFakultas";	
	}

}
