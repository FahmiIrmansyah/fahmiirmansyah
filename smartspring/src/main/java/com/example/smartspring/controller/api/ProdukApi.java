package com.example.smartspring.controller.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.smartspring.model.ProdukModel;
import com.example.smartspring.service.ProdukService;

@RestController
@RequestMapping("/api/ProdukApi")
public class ProdukApi {

	@Autowired
	ProdukService produkservice;
	
	@GetMapping("/get")
	@ResponseStatus(code=HttpStatus.OK)
	public List<ProdukModel> getApi(){
		List<ProdukModel> listProdukModel = new ArrayList<ProdukModel>();
		listProdukModel = this.produkservice.tampilProduk();
		return listProdukModel;
	}
	
	
}
