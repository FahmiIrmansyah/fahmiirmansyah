package com.example.smartspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.smartspring.model.SuperheroModel;
import com.example.smartspring.repository.SuperheroRepository;

@Service
@Transactional
public class SuperheroService {

	@Autowired
	private SuperheroRepository sr;
	
	public void create(SuperheroModel sm) {
		sr.save(sm);
	}
	
	public void update(SuperheroModel sm) {
		sr.save(sm);
	}
	
	public List<SuperheroModel> read (){
		return sr.findAll();
	}
	
	public void delete(String nama) {
		sr.deleteById(nama);
	}
}
