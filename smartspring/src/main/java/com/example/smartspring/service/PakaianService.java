package com.example.smartspring.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.smartspring.model.PakaianModel;
import com.example.smartspring.repository.PakaianRepository;

@Service
@Transactional
public class PakaianService {
	
	@Autowired
	PakaianRepository pr;

	public void create(PakaianModel pakaianModel) {
		pr.save(pakaianModel);
	}
}
