package com.example.smartspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.smartspring.model.PlayerModel;
import com.example.smartspring.repository.PlayerRepository;

@Service
@Transactional
public class PlayerService {

	@Autowired
	PlayerRepository pr;
	
	public void create(PlayerModel pm) {
		pr.save(pm);
	}
	
	public List<PlayerModel> read() {
		return pr.findAll();
	}
	
}
