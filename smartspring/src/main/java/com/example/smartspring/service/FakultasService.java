package com.example.smartspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.smartspring.model.FakultasModel;
import com.example.smartspring.repository.FakultasRepository;

@Service
@Transactional
public class FakultasService {
	
	@Autowired
	FakultasRepository fr;
	
	public void create(FakultasModel fakultasModel) {
		fr.save(fakultasModel);
	}
	
	
	
	 public List<FakultasModel> read (){ 
		 return fr.findAll(); 
		 }
	 
	 

}
