package com.example.smartspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.smartspring.model.PresidenModel;
import com.example.smartspring.repository.PresidenRepository;

@Service
@Transactional
public class PresidenService {

	@Autowired
	private PresidenRepository presidenRepository;
	
	public void create(PresidenModel presidenModel) {
		presidenRepository.save(presidenModel);
	}
	
	public List<PresidenModel> read(){
		return presidenRepository.findAll();
	}
	
	public List<PresidenModel> read2(){
		return presidenRepository.querySelectUsia70atauMengandungHuruf0();
	}
}
