package com.example.smartspring.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.smartspring.model.DosenModel;
import com.example.smartspring.repository.DosenRepository;

@Service
@Transactional
public class DosenService {
	
	@Autowired
	private DosenRepository dosenRepository;
	//modifier kosong namaMEthod(TipeVariabel namaVariabel)
	public void create(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
	public void update(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
	public List<DosenModel> read(){
		return dosenRepository.findAll();
	}
	
	public List<DosenModel> readOrderNama() {
		return dosenRepository.querySelectAllOrderNamaDesc();
	}
	
	public List<DosenModel> readOrderBy() {
		return dosenRepository.querySelectAllOrderUsiaAsc();
	}
	
	public List<DosenModel> readWhereNama(String katakunci_nama){
		return dosenRepository.querySelectAllWhereNamaLike(katakunci_nama);
	}
	
	public List<DosenModel> readWhereUsiaByOperator(int katakunci_usia,String katakunci_operator,String katakunci_awalan){
		List<DosenModel>dosenModelList = new ArrayList<DosenModel>();
		
		if(katakunci_operator.equals("=")) {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaEqual(katakunci_usia,katakunci_awalan);
		} else if (katakunci_operator.equals(">")){
			dosenModelList = dosenRepository.querySelectAllWhereUsiaMoreThanEqual(katakunci_usia,katakunci_awalan);
		} else if (katakunci_operator.equals(">=")){
			dosenModelList = dosenRepository.querySelectAllWhereUsiaLessThan(katakunci_usia,katakunci_awalan);
		} else if (katakunci_operator.equals("<")){
			dosenModelList = dosenRepository.querySelectAllWhereUsiaLessThanEqual(katakunci_usia,katakunci_awalan);
		} else {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaNotEqual(katakunci_usia,katakunci_awalan);
		} 
		return dosenModelList;
	}
	
	public DosenModel readByID(String namaDosen) {
		return dosenRepository.queryWhereNamaDosen(namaDosen);
	}
	
	public void delete(String namaDosen) {
		dosenRepository.deleteById(namaDosen);
	}
	
	public List<DosenModel> readUsiaDuaLima(){
		return dosenRepository.queryWhereSelectUsiaLessThan();
	}

}
