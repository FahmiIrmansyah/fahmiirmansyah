package com.example.smartspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.smartspring.model.ProdukModel;
import com.example.smartspring.repository.ProdukRepository;

@Service
@Transactional
public class ProdukService {
	
	@Autowired
	private ProdukRepository produkRepository;
	
	public void create(ProdukModel produkModel) {
		produkRepository.save(produkModel);
	}
	
	public List<ProdukModel> tampilProduk (){
		return produkRepository.findAll();
		}
	
	public List<ProdukModel> cariProduk (String cariProduk){
		return produkRepository.cariNamaProduk(cariProduk);
	}
	
	public List<ProdukModel> cariKode (String cariKode){
		return produkRepository.cariNamaKode(cariKode);
	}	
	
	public List<ProdukModel> urutHarga(){
		return produkRepository.hargaProdukAsc();
	}

}
