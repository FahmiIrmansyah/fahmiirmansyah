package com.example.smartspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.smartspring.model.PasienModel;
import com.example.smartspring.repository.PasienRepository;

@Service
@Transactional
public class PasienService {

	// Autowired untuk koneksi ke repository
	@Autowired
	private PasienRepository pasienRepository;

	// method untuk create
	public void create(PasienModel pasienModel) {
		this.pasienRepository.save(pasienModel);
	}

	// method untuk read data
	public List<PasienModel> read() {
		return this.pasienRepository.reporead();
	}

	public List<PasienModel> searchNama(String namaPasien) {
		return this.pasienRepository.searchNamaPasien(namaPasien);	
		
	}

	// pola service
	// public outputnya apa, nama methodnya (Tipe inputnya)
}
